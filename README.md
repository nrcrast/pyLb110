# PyLb110
A Python library for controlling Tp-Link LB110 Smart Bulbs!

# Usage
```python
tp_link = TpLink(TOKEN)
livingroom_left = tp_link.get_bulb_by_alias('Living Room Left')
if livingroom_left:
	livingroom_left.on()
```

# Finding Your Token
I suggest you grab an app (eg. Packet Capture) to sniff traffic from your phone to the Tp-Link servers. Looking at the headers, it should be fairly easy to spot your token.