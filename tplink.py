import requests
import os
import sys
import json
import pprint

class Bulb:
	def __init__(self, token, device_id, alias=None, model=None):
		self.device_id = device_id
		self.token = token
		self.alias = alias
		self.model = model

	def change_bulb_brightness(self, brightness, transition):
		if brightness == 0:
			bulb_state = {'smartlife.iot.smartbulb.lightingservice':{'transition_light_state':{'on_off':0, 'transition_period':transition}}}
		else:
			bulb_state = {'smartlife.iot.smartbulb.lightingservice':{'transition_light_state':{'on_off':1, 'brightness':brightness, 'transition_period':transition}}}

		payload = {}
		payload['method'] = 'passthrough' # Pass request right through to device (I'm assuming)
		payload['params'] = {'deviceId': self.device_id, 'requestData': json.dumps(bulb_state)}
		r = requests.post('https://wap.tplinkcloud.com?token={}'.format(self.token), json=payload)
		return r.status_code == requests.codes.ok

	def on(self, transition = 10):
		return self.change_bulb_brightness(100, transition)

	def off(self, transition = 10):
		return self.change_bulb_brightness(0, transition)

class TpLink:
	def __init__(self, token):
		self.token = token

	def get_all_bulbs(self):
		bulbs = []
		payload = {'method':'getDeviceList', 'params':{}}
		r = requests.post('https://wap.tplinkcloud.com?token={}'.format(self.token), json=payload)

		if r.status_code == requests.codes.ok:
			result = json.loads(r.text)
			device_list = result.get('result', {}).get('deviceList', {})

			for device in device_list:
				if device['deviceType'] == 'IOT.SMARTBULB':
					bulbs.append(Bulb(self.token, device['deviceId'], device['alias'], device['deviceModel']))

		return bulbs

	def get_bulb_by_id(self, bulb_id):
		return Bulb(self.token, bulb_id)

	def get_bulb_by_alias(self, bulb_alias):
		bulbs = self.get_all_bulbs()

		for bulb in bulbs:
			if bulb.alias == bulb_alias:
				return bulb

		return None